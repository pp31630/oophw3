import java.util.Scanner;

public class POOUI {
	private static boolean isNumber(String str) {
		if(str.length() == 0) return false;
		for(int i = 0; i < str.length(); i++)
			if(!Character.isDigit(str.charAt(i)))
				return false;
		return true;
	}
	
	private static String firstWord(String s) {
		int index = s.indexOf(' ');
		if (index == -1) return s;
		return s.substring(0, index);
	}
	
	private static int getInteger(int lower, int upper) {
		Scanner scanner = new Scanner(System.in);
		String in = scanner.nextLine();
		while(!isNumber(firstWord(in)) || Integer.parseInt(firstWord(in)) < lower || Integer.parseInt(firstWord(in)) > upper) {
			System.out.print("Please enter again: ");
			in = scanner.nextLine();
		}
		return Integer.parseInt(firstWord(in));
	}
	
	public static int artCommand() {
		String choose;
		Scanner scanner = new Scanner(System.in);
		System.out.print("Enter command: ");
		choose = scanner.next();
		while(true) {
			if(choose.equals("r"))	return 0;
			if(choose.equals("q")) 	return -1;
			if(choose.equals("p"))	return -2;
			if(choose.equals("b"))	return -3;
			if(choose.equals("a"))	return -4;
			System.out.print("Please enter again: ");
			choose = scanner.next();
		}
	}
	
	public static int homeCommand(POODirectory dir) {
		String choose;
		Scanner scanner = new Scanner(System.in);
		System.out.print("Enter command: ");
		choose = scanner.next();
		while(true) {
			if(choose.equals("q"))	return -1;
			if(isNumber(choose) && (Integer.parseInt(choose) == 1 || Integer.parseInt(choose) == 2)) break;
			System.out.print("Please enter again: ");
			choose = scanner.next();
		}
		return Integer.parseInt(choose);
	}
	
	public static int allboardCommand(POODirectory dir) {
		String choose;
		Scanner scanner = new Scanner(System.in);
		System.out.print("Enter command: ");
		choose = scanner.next();
		while(true) {
			if(choose.equals("q"))	return -1;
			if(choose.equals("n"))	return -2;
			if(isNumber(choose) && Integer.parseInt(choose) >= 0 && Integer.parseInt(choose) <= dir.length()) break;
			System.out.print("Please enter again: ");
			choose = scanner.next();
		}
		return Integer.parseInt(choose);
	}
	
	public static int boardCommand(POOBoard board) {
		String choose;
		Scanner scanner = new Scanner(System.in);
		System.out.print("Enter command: ");
		choose = scanner.next();
		while(true) {
			if(choose.equals("q")) 	return -1;
			if(choose.equals("p"))	return -2;
			if(choose.equals("d"))	return -3;
			if(choose.equals("m"))	return -4;
			if(isNumber(choose) && Integer.parseInt(choose) >= 0 && Integer.parseInt(choose) <= board.length()) break;
			System.out.print("Please enter again: ");
			choose = scanner.next();
		}
		return Integer.parseInt(choose);
	}
	
	public static int dirCommand(POODirectory dir) {
		String choose;
		Scanner scanner = new Scanner(System.in);
		System.out.print("Enter command: ");
		choose = scanner.next();
		while(true) {
			if(choose.equals("q"))	return -1;
			if(choose.equals("a"))	return -2;
			if(choose.equals("g"))	return -3;
			if(choose.equals("l")) 	return -4;
			if(choose.equals("d"))	return -5;
			if(choose.equals("m"))	return -6;
			if(isNumber(choose) && Integer.parseInt(choose) >= 0 && Integer.parseInt(choose) <= dir.length()
				&& (Integer.parseInt(choose) == 0 || dir.getCollection(Integer.parseInt(choose)).type != 2)) break;
			System.out.print("Please enter again: ");
			choose = scanner.next();
		}
		return Integer.parseInt(choose);
	}
	
	public static String inputBoardName() {
		Scanner scanner = new Scanner(System.in);
		System.out.print("Enter the new board name: ");
		return scanner.nextLine();
	}
	
	public static String inputDirName() {
		Scanner scanner = new Scanner(System.in);
		System.out.print("Enter the new directory name: ");
		return scanner.nextLine();
	}
	
	public static int inputDel(int length) {
		System.out.print("What do you want to delete? ");
		return getInteger(1, length);
	}
	
	public static int inputSrc(int length) {
		System.out.print("What do you want to move? ");
		return getInteger(1, length);
	}
	
	public static int inputDes(int length) {
		System.out.print("Where do you want to move to? ");
		return getInteger(1, length);
	}
	
	public static boolean articleMax() {
		if(POOArticle.totalart == POOArticle.MAXART) {
			System.out.print("Reached maximum article number.");
			return true;
		}
		return false;
	}
	
	public static String inputTitle() {
		Scanner scanner = new Scanner(System.in);
		System.out.print("Title: ");
		return scanner.nextLine();
	}
	
	public static String inputAuthor() {
		Scanner scanner = new Scanner(System.in);
		System.out.print("Author: ");
		return scanner.nextLine();
	}
	
	public static String inputContent() {
		Scanner scanner = new Scanner(System.in);
		System.out.print("Content: ");
		return scanner.nextLine();
	}
	
	public static int inputEvalType() {
		System.out.print("Enter 1. Push 2. Boo 3. Arrow : ");
		return getInteger(1, 3);
	}
	
	public static String inputEval() {
		Scanner scanner = new Scanner(System.in);
		System.out.print("Enter evaluation: ");
		return scanner.nextLine();
	}
	
	public static void boardNotExist() {
		System.out.println("Board not exists.");
	}
	
	public static void noBoardExist() {
		System.out.println("No board exists.");
	}
	
	public static void noArtExist() {
		System.out.println("No articles exists.");
	}
	
	public static void noBoardDirExist() {
		System.out.println("No board or directory exists.");
	}
}