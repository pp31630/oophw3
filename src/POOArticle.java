public class POOArticle {
	private int id;
	private String title;
	private String author;
	private String content;
	private int evalcnt;
	private int totaleval;
	private byte[] evaltype = new byte[MAXEVAL];
	private String[] evalmsg = new String[MAXEVAL];
	private static final int MAXEVAL = 1024;
	public static final int MAXART = 899;
	public static int totalart;
	
	public POOArticle(String title, String author, String content) {
		this.id = totalart;
		this.title = title;
		this.author = author;
		this.content = content;
		totalart++;
	}
	
	public void push(String msg) {
		if(totaleval < MAXEVAL) {
			evalmsg[totaleval] = msg;
			evaltype[totaleval] = 1;
			totaleval++;
			evalcnt++;
		}
	}
	
	public void boo(String msg) {
		if(totaleval < MAXEVAL) {
			evalmsg[totaleval] = msg;
			evaltype[totaleval] = -1;
			totaleval++;
			evalcnt--;
		}
	}
	
	public void arrow(String msg) {
		if(totaleval < MAXEVAL) {
			evalmsg[totaleval] = msg;
			evaltype[totaleval] = 0;
			totaleval++;
		}
	}
	
	public void show() {
		System.out.print("\f");
		for(int i = 0; i < 18-(title.length()+author.length())/2; i++)
			System.out.print(" ");
		System.out.println("\"" + title + "\" by " + author + " (" + (id+100) + ")");
		System.out.println("===============================================");
		System.out.println("[p] push [b] boo [a] arrow [r] return [q] quit");
		System.out.println("===============================================");
		System.out.println("Content: " + content);
		System.out.println("Evaluation count: " + evalcnt);
		System.out.println("--");
		for(int i = 0; i < totaleval; i++) {
			if(evaltype[i] == 1)		System.out.print("push:  ");
			else if(evaltype[i] == -1)	System.out.print("Boo:   ");
			else if(evaltype[i] == 0)	System.out.print("Arrow: ");			
			System.out.println(evalmsg[i]);
		}
	}
	
	public void list() {
		System.out.print(evalcnt + "\t\t" + author);
		for(int i = 0; i < 16-author.length(); i++)
			System.out.print(" ");
		System.out.println(title);
	}
	
	public String getTitle() {
		return title;
	}
}