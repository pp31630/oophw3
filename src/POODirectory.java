public class POODirectory {
	private String name;
	private int colcnt;
	private POOCollection[] col = new POOCollection[MAXCOL];
	private static final int MAXCOL = 1024;
	public boolean home;
	public boolean fixed;
	public boolean allboard;
	public static int totaldir;
	
	public POODirectory(String name){
	//create a directory with the name
		this.name = name;
		this.home = false;
		this.fixed = false;
		this.allboard = false;
		totaldir++;
	}
	
	public POODirectory(String name, boolean home, boolean fixed, boolean allboard) {
		this(name);
		this.home = home;
		this.fixed = fixed;
		this.allboard = allboard;
	}
	
	public void add(POOBoard board){
	//append the board to the directory
		col[colcnt] = new POOCollection();
		col[colcnt].board = board;
		col[colcnt].type = 0;
		colcnt++;
	}
	
	public void add(POODirectory dir){
	//append the directory to the directory
		col[colcnt] = new POOCollection();
		col[colcnt].dir = dir;
		col[colcnt].type = 1;
		colcnt++;
	}
	
	public void add_split(){
	//append a splitting line to the directory
		col[colcnt] = new POOCollection();
		col[colcnt].type = 2;
		colcnt++;
	}
	
	public void del(int pos){
	//delete the board/directory/splitting line at position pos
		pos--;
		for(int i = pos; i < colcnt; i++)
			col[i] = col[i+1];
		colcnt--;
	}
	
	public void move(int src, int dest){
	//move the board/directory/splitting line at position src to position dest
		src--; dest--;
		POOCollection tmp = col[src];
		
		if(dest > src) {
			for(int i = src; i < dest; i++)
				col[i] = col[i+1];
		}
		else if(dest < src) {
			for(int i = src; i > dest; i--)
				col[i] = col[i-1];
		}
		
		col[dest] = tmp;
	}
	
	public int length(){
	//get the current number of items in the directory
		return colcnt;
	}
	
	public void show(){
	//show the board/directory titles and splitting lines of the directory
		System.out.print("\f");
		for(int i = 0; i < 24-name.length()/2; i++)
			System.out.print(" ");
		System.out.println(name);
		System.out.println("===============================================");
		if(home == false) {
			if(allboard == true) System.out.println("            [n] new board [q] quit");
			else { 	System.out.println(" [a] add board [g] add directory [l] add split");
					System.out.println(" [d] delete    [m] move          [q] quit"); }
			System.out.println("===============================================");
		}
		if(home == false) System.out.println("0.\t[D] ../");
		for(int i = 0; i < colcnt; i++)
			if(col[i].type == 0)		System.out.println(i+1 + ".\t[B] " + col[i].board.getName());
			else if(col[i].type == 1)	System.out.println(i+1 + ".\t[D] " + col[i].dir.getName());
			else						System.out.println(i+1 + ".\t---------------------------------------");
	}
	
	public String getName() {
		return name;
	}
	
	public POOCollection getCollection(int pos) {
		return col[pos-1];
	}
}