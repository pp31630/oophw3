public class POOBoard {
	private String name;
	private POODirectory last;
	private int postcnt;
	private POOCollection[] col = new POOCollection[MAXPOST];
	private static final int MAXPOST = 1024;
	public static int totalboard;
	
	public POOBoard(String name) {
	//create a board with the name
		this.name = name;
		totalboard++;
	}
	
	public void add(POOArticle article) {
	//append the article to the board
		col[postcnt] = new POOCollection();
		col[postcnt].art = article;
		col[postcnt].type = 3;
		postcnt++;
	}
	
	public void del(int pos) {
	//delete the article at position pos
		pos--;
		for(int i = pos; i < postcnt; i++)
			col[i] = col[i+1];
		postcnt--;
	}
	
	public void move(int src, int dest) {
	//move the article at position src to position dest
		src--; dest--;
		POOCollection tmp = col[src];
		
		if(dest > src) {
			for(int i = src; i < dest; i++)
				col[i] = col[i+1];
		}
		else if(dest < src) {
			for(int i = src; i > dest; i--)
				col[i] = col[i-1];
		}
		
		col[dest] = tmp;
	}
	
	public int length() {
	//get the current number of articles in the board
		return postcnt;
	}
	
	public void show() {
	//show the article titles of the board
		System.out.print("\f");
		for(int i = 0; i < 24-name.length()/2; i++)
			System.out.print(" ");
		System.out.println(name);
		System.out.println("===============================================");
		System.out.println("    [p] post [d] delete [m] move [q] quit");
		System.out.println("===============================================");
		System.out.println("Number\tEvaluation\tAuthor\t\tTitle");
		System.out.println("0.\t[D] ../");
		for(int i = 0; i < postcnt; i++) {
			System.out.print(i+1 + ".\t");
			col[i].art.list();
		}
	}
	
	public String getName() {
		return name;
	}
	
	public POOCollection getArt(int pos) {
		return col[pos-1];
	}
}