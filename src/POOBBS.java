public class POOBBS {
	public static void main(String[] args) {
		POOArticle[] art = new POOArticle[899];
		POOBoard[] board = new POOBoard[10000];
		POODirectory[] dir = new POODirectory[10000];
		POOCollection current = new POOCollection();
		POOCollection tmp;
		POODirectory allboard;
		POOArticle article;
		String boardname;
		boolean flag, change = true;
		int command, i;
	
		dir[0] = new POODirectory("Home", true, true, false);
		current.board = null;
		current.dir = dir[0];
		current.type = 1;
		dir[1] = new POODirectory("My Favorite", false, true, false);
		dir[2] = new POODirectory("All Boards", false, true, true);
		current.dir.add(dir[1]);
		current.dir.add(dir[2]);
		allboard = dir[2];
		
		while(true) {
			if(current.type == 1 && current.dir.home == true) {
				current.dir.show();
				command = POOUI.homeCommand(current.dir);
				if(command == -1) break;
				tmp = current; current = current.dir.getCollection(command); current.last = tmp;
			}
			else if(current.type == 1 && current.dir.allboard == true) {
				current.dir.show();
				command = POOUI.allboardCommand(current.dir);
				if(command == -1) break;
				if(command == 0)  { current = current.last; continue; }
				if(command == -2) {
					board[POOBoard.totalboard] = new POOBoard(POOUI.inputBoardName());
					current.dir.add(board[POOBoard.totalboard-1]);
				}
				else { tmp = current; current = current.dir.getCollection(command); current.last = tmp; }
			}
			else if(current.type == 0) {
				if(change)	current.board.show();
				else		change = true;
				command = POOUI.boardCommand(current.board);
				if(command == -1) break;
				if(command == 0)  { current = current.last; continue; }
				if(command == -2) {
					if(!POOUI.articleMax()) {
						art[POOArticle.totalart] = new POOArticle(POOUI.inputTitle(), POOUI.inputAuthor(), POOUI.inputContent());
						current.board.add(art[POOArticle.totalart-1]);
					}
				}
				else if(command == -3) {
					if(current.board.length() != 0)
						current.board.del(POOUI.inputDel(current.board.length()));
					else { POOUI.noArtExist(); change = false; }
				}
				else if(command == -4) {
					if(current.board.length() != 0)
						current.board.move(POOUI.inputSrc(current.board.length()), POOUI.inputDes(current.board.length()));
					else { POOUI.noArtExist(); change = false; }
				}
				else { tmp = current; current = current.board.getArt(command); current.last = tmp; }
			}
			else if(current.type == 1) {
				if(change) 	current.dir.show();
				else		change = true;
				command = POOUI.dirCommand(current.dir);
				if(command == -1) break;
				if(command == 0)  { current = current.last; continue; }
				if(command == -2) {
					if(POOBoard.totalboard != 0) {
						boardname = POOUI.inputBoardName();
						flag = false;
						while(!flag) {
							for(i = 0; i < POOBoard.totalboard; i++)
								if((board[i].getName()).equals(boardname)) {
									board[POOBoard.totalboard] = board[i];
									current.dir.add(board[POOBoard.totalboard-1]);
									flag = true;
									break;
								}
							if(flag) break;
							POOUI.boardNotExist();
							boardname = POOUI.inputBoardName();
						}
					}
					else { POOUI.noBoardExist(); change = false; }
				}
				else if(command == -3) {
					dir[POODirectory.totaldir] = new POODirectory(POOUI.inputDirName());
					current.dir.add(dir[POODirectory.totaldir-1]);
				}
				else if(command == -4) current.dir.add_split();
				else if(command == -5) {
					if(current.dir.length() != 0)
						current.dir.del(POOUI.inputDel(current.dir.length()));
					else { POOUI.noBoardDirExist(); change = false; }
				}
				else if(command == -6) {
					if(current.dir.length() != 0)
						current.dir.move(POOUI.inputSrc(current.dir.length()), POOUI.inputDes(current.dir.length()));
					else { POOUI.noBoardDirExist(); change = false; }
				}
				else { tmp = current; current = current.dir.getCollection(command); current.last = tmp; }
			}
			else if(current.type == 3) {
				current.art.show();
				command = POOUI.artCommand();
				if(command == -1) break;
				if(command == 0)  { current = current.last; continue; }
				if(command == -2) 		current.art.push(POOUI.inputEval());
				else if(command == -3)	current.art.boo(POOUI.inputEval());
				else if(command == -4) 	current.art.arrow(POOUI.inputEval());
			}
		}	
	}
}